/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package battleship;

import com.sun.prism.paint.Color;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ItemEvent;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.JTextField;

import java.util.Scanner;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

/**
 *
 * @author ftonye
 */
public class BattleSea extends JFrame {

    private static JButton[] listbtn;
    private static ArrayList<Ship> listShip;
    private static ArrayList<JButton> listJButtonInShip;
    public static int stat;
    private JRadioButton client;
    private JRadioButton server;
    private ButtonGroup bG;
    private InetAddress adrs;

    private JButton btnStartServer;
    private JTextField txtPortNumber;
    private JPanel mainframe;
    private JButton btnStartClient;
    // private JTextField txtPortNumber;
    private JTextField txtServerIP;

    private ClientBackGroundCom com1;

    private BackGroundCom com;
    private Thread t;

    public BattleSea() {
        //this.setSize(500,425);
        try {
            adrs = InetAddress.getLocalHost();
        } catch (UnknownHostException ex) {
            Logger.getLogger(BattleSea.class.getName()).log(Level.SEVERE, null, ex);
        }

        this.setTitle("BattleSea Server address is: " + adrs.getHostAddress());
        this.setLayout(new FlowLayout());

        createGrid();

        CreateRadioButton();

        CreateShips();

    }

    private void CreateRadioButton() {

        client = new JRadioButton("Client");
        server = new JRadioButton("Server");
        bG = new ButtonGroup();

        bG.add(client);
        bG.add(server);

        this.add(client);
        this.add(server);

        txtPortNumber = new JTextField();
        txtServerIP = new JTextField();
        txtPortNumber.setText("25000");
        txtServerIP.setText(adrs.getHostAddress());

        this.add(btnStartClient);
        this.add(btnStartServer);
        this.add(txtServerIP);
        this.add(txtPortNumber);

        txtServerIP.setVisible(false);
        btnStartClient.setVisible(false);
        btnStartServer.setVisible(false);
        txtPortNumber.setVisible(false);

        ItemListener itemListener = (ItemEvent e) -> {
            if (e.getStateChange() == ItemEvent.SELECTED) {

                txtServerIP.setVisible(true);
                btnStartClient.setVisible(true);
                txtPortNumber.setVisible(true);
                btnStartServer.setVisible(false);
                //rdioButton client selected
            } else if (e.getStateChange() == ItemEvent.DESELECTED) {
                //radio button client not selected

                txtServerIP.setVisible(false);
                btnStartClient.setVisible(false);
                btnStartServer.setVisible(true);

            }
        };
        client.addItemListener(itemListener);

    }
    // @Override

    private void createGrid() {
        mainframe = new JPanel(new GridLayout(10, 10));
        // mainframe.setLayout(new GridLayout(8,8));
        listbtn = new JButton[100];

        for (int i = 0; i < listbtn.length; i++) {
            listbtn[i] = new JButton(String.valueOf(i));
            //listbtn[i].setSize(20,20);
            listbtn[i].setBackground(java.awt.Color.blue);
            mainframe.add(listbtn[i]);
            //this.add(listbtn[i]);

        }
        this.getContentPane().add(mainframe);

        btnStartServer = new JButton("Start Server");
        btnStartServer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                linkListenerToSeaSector();
                com = new BackGroundCom(Integer.parseInt(txtPortNumber.getText().toString()));
                t = new Thread(com);
                t.start();
            }

        });

        btnStartClient = new JButton("Start Client");
        btnStartClient.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                linkListenerToSeaSector1();
                com1 = new ClientBackGroundCom(Integer.parseInt(txtPortNumber.getText().toString()), txtServerIP.getText());
                t = new Thread(com1);
                t.start();
            }

        });

    }

    private void linkListenerToSeaSector() {
        for (int i = 0; i < listbtn.length; i++) {
            listbtn[i].addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {

                    com.missileOutgoing = Integer.parseInt(ae.getActionCommand());
                    com.feedBack= com.missileOutgoing;
                    com.dataToSend = true;

                }
            });
        }

    }

    private void linkListenerToSeaSector1() {
        for (int i = 0; i < listbtn.length; i++) {
            listbtn[i].addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {

                    com1.msgToSend = Integer.parseInt(ae.getActionCommand());
                   com1.feedBack= com1.msgToSend;

                    com1.dataToSend = true;

                }
            });
        }

    }
private boolean postionShip(int head,int nb,int position,int cnt,int valeur)
{
    boolean verif=false;
    if(position==1)
    {
            if ((head+(cnt*10) > 100)||valeur==(head + cnt * 10) || valeur==(head +(cnt* 10) + 1) || valeur==(head + (cnt * 10) - 1)  || valeur==(head + (cnt*10) + 10) || valeur==(head - 10)) 
                   {
                    verif= true;   

                   }
      
                  

    }
    else if(position==0)
    {
            
              if ((head+cnt)>100||valeur==(head + cnt) || valeur==(head + cnt + 10) || valeur==(head + cnt - 10) || valeur==(head - 1) || valeur==(head + cnt + 1)) 

            { 
               verif= true; 

            }
               
           
   }       
     return verif;
 }




    private void CreateShips() {
        int head=0, nb = 0;
        boolean verif;
        Random r = new Random();
        
        listShip = new ArrayList<Ship>();
        listJButtonInShip = new ArrayList<JButton>();
        int lineTail,lineHead,columnHead,columnTail,position=0;
     
        for (int i = 0; i < 5; i++) {
            
       verif=true;
       while(verif)
       {
                 position=r.nextInt(2);
                 head = r.nextInt(99) + 1;
                 nb = r.nextInt(4) + 2;
                 lineTail = (head + nb) / 10;
                 lineHead = (head) / 10;
                 columnHead = head % 10;
                 columnTail = (head + (10 * nb)) % 10;
                 
                
               if( (columnTail == columnHead) && (lineTail == lineHead)&& (head+nb<100)&& (head+nb*10<100))
               {
             

                   verif=false;
                   for(JButton element:listJButtonInShip)
                   {
                       for (int k = 0; k < nb; k++)
                       {
                           if((postionShip( head, nb, position,k, Integer.parseInt(element.getText()))))
                           {
                               verif=true;
                           }
                           
                           
                       }
                   }
               }
         else
               {verif=true;}
             
              
         }
            
                System.out.println(Integer.toString(position) );
                ArrayList<JButton> boatsection = new ArrayList<JButton>();
                if(position==1)
                {
                
                       for (int j = 0; j < nb; j++) {
                       System.out.println(Integer.toString(head + j*10));

                        boatsection.add(listbtn[head + (j * 10)]);

                        listJButtonInShip.add(listbtn[head + (j * 10)]);
                    }
                     Ship s = new Ship(boatsection);
                    listShip.add(s);
                }
                else if(position==0)
                {
                    
                      for (int j = 0; j < nb; j++) {

                        boatsection.add(listbtn[head + j]);

                        listJButtonInShip.add(listbtn[head + j]);
                        System.out.println(Integer.toString(head + j));
                    }
                    Ship s = new Ship(boatsection);
                    listShip.add(s);
                }
                       
        }
              
} 
      

   

    public static int UpdateGrig(int incomming,int feedback,int status) {
        Boolean missHit = true;
        Ship sh2=new Ship();
        LineBorder hitenBtnborder= new  LineBorder(java.awt.Color.gray,6);
        LineBorder deadBtnborder= new  LineBorder(java.awt.Color.black,6);

        boolean catchShipTarget =false;
     
            for (Ship element : listShip) {
               catchShipTarget= element.checkHit(listbtn[incomming]);
               if (catchShipTarget)
               {
               
               sh2=(Ship)element;
               }
                //element.checkShipDistraction();
            }
      stat=sh2.getStatus();
        
      if(stat==2)
      { for (JButton element : sh2.BoatSections) {
               
                         element.setBackground(java.awt.Color.black);

            }
      }
        for (JButton element : listJButtonInShip) {
            
            if (element.equals(listbtn[incomming])) {
                missHit = false;
               // Ship.status=1;
            }
            
         
        }
        
       switch  (status)
       {
           
            case 0:
                  listbtn[feedback].setForeground(java.awt.Color.yellow);
             break;
             
           case 1:
              if(  listbtn[feedback].getBackground()==(java.awt.Color.red)||listbtn[feedback].getBackground()==(java.awt.Color.green))
                {
                   listbtn[feedback].setBorder(hitenBtnborder);
                }  
            
              else 
                 {listbtn[feedback].setBackground(java.awt.Color.gray);}
             break;
           case 2:
                  
                if(  listbtn[feedback].getBackground()==(java.awt.Color.red)||listbtn[feedback].getBackground()==(java.awt.Color.green))
                
               {
                   listbtn[feedback].setBorder(deadBtnborder);
                } 
               
               
                else{
               listbtn[feedback].setBackground(java.awt.Color.black);
              
                   }
                
               
             break;
         
      
       } 
        
     

        if (missHit) {
            listbtn[incomming].setBackground(java.awt.Color.magenta);
        }
        
        
        return stat;
    }
    private static void colorDistructedShip(int feedBack)
    {
         LineBorder hitenBtnborder= new  LineBorder(java.awt.Color.gray,6);
         LineBorder deadBtnborder= new  LineBorder(java.awt.Color.black,6);

                for(int i= feedBack;i<10;i++)
                {
                  if(listbtn[i].getBackground()== java.awt.Color.gray|| listbtn[i].getBorder()==hitenBtnborder)
                  {
                     listbtn[i].setBorder(deadBtnborder);
                  }
                  else
                  {i=10;}
                }
                 for(int i= feedBack;i<feedBack-10;i--)
                {
                  if(listbtn[i].getBackground()== java.awt.Color.gray|| listbtn[i].getBorder()==hitenBtnborder)
                  {
                      listbtn[i].setBorder(deadBtnborder);
                  }
                  else
                  {i=feedBack-10;}
                }
                
                for(int i= feedBack;i< feedBack+100;i+=10)
                {
                  if(listbtn[i].getBackground()== java.awt.Color.gray|| listbtn[i].getBorder()==hitenBtnborder)
                  {
                     listbtn[i].setBorder(deadBtnborder);
                  }
                  else
                  {i=feedBack+100;}
                }
                 for(int i= feedBack;i<feedBack-100;i-=10)
                {
                  if(listbtn[i].getBackground()== java.awt.Color.gray|| listbtn[i].getBorder()==hitenBtnborder)
                  {
                      listbtn[i].setBorder(deadBtnborder);
                  }
                  else
                  {i=feedBack-100;}
                }
    }
    private static void drownShip(Ship myShip)
    {
    
          for (JButton element : myShip.BoatSections) {
               
                         element.setBackground(java.awt.Color.black);

            }
    
    }
}
